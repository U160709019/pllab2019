import java.util.*;
import java.io.*;
public class a{
    
    public static String[] doSelectionSort(String[] arr) {
		for (int i=0; i<arr.length-1; i++) {
		     for (int j=i+1; j<arr.length; j++) {
		        if (arr[i].compareTo(arr[j]) > 0) {
		           String temp=arr[j]; 
		           arr[j]=arr[i]; 
		           arr[i]=temp;
		        }
		     }
		  }
		return arr;
        }


        public static String[] mergeSort(String[] names) {
            if (names.length >= 2) {
                String[] left = new String[names.length / 2];
                String[] right = new String[names.length - names.length / 2];
    
                for (int i = 0; i < left.length; i++) {
                    left[i] = names[i];
                }
    
                for (int i = 0; i < right.length; i++) {
                    right[i] = names[i + names.length / 2];
                }
    
                mergeSort(left);
                mergeSort(right);
                merge(names, left, right);
            }
            return names;
        }

        public static void merge(String[] names, String[] left, String[] right) {
            int a = 0;
            int b = 0;
            for (int i = 0; i < names.length; i++) {
                if (b >= right.length || (a < left.length && left[a].compareToIgnoreCase(right[b]) < 0)) {
                    names[i] = left[a];
                    a++;
                } else {
                    names[i] = right[b];
                    b++;
                }
            }
        }
        



    public static void main(String[] args) throws FileNotFoundException, ArrayIndexOutOfBoundsException{
    
        String infile = args[0];
        Scanner s = new Scanner(new File(infile));
        String[] arr= null;
	    List<String> itemsSchool = new ArrayList<String>();
 
	        

	        while (s.hasNext()) 
	        { 
	            
	          
	                itemsSchool.add(s.next());
	            
	        }

	        arr = (String[])itemsSchool.toArray(new String[itemsSchool.size()]);
	        long startTime = System.nanoTime();
	        String[] selectionSortedArr=doSelectionSort(arr);
	        Collections.reverse(Arrays.asList(selectionSortedArr));
	        long endTime = System.nanoTime();
	        long timeElapsed = endTime - startTime;
	        System.out.println("selection sorting Time:"+timeElapsed+" nano second");
	        /*for (String item : selectionSortedArr) {
				System.out.println(item);
			}*/
	        
	        
	        startTime = System.nanoTime();
	        String[] mergeSortedArr=mergeSort(arr);
	        Collections.reverse(Arrays.asList(mergeSortedArr)); 
	        endTime = System.nanoTime();
	        timeElapsed = endTime - startTime;
	        System.out.println("merge sorting time:"+timeElapsed+" nano second");
	        /*for (String item : mergeSortedArr) {
				System.out.println(item);
			}*/
	        
	        
	        
	    
	    
	}

           
    
}
