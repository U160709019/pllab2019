import time
import sys
from timeit import timeit

f1 = open(sys.argv[1],'r')
#file pathi : C:\Users\rozpa\Desktop\stringss.txt
unsort_list = []

for line in f1:
    unsort_list.append(line)

unsort_list = [x.strip() for x in unsort_list]
#print(unsort_list)
#eğer neyi sıraladığımızı görmek istersek print önündeki komut satırını kaldırabiliriz.

def bubblesort(unsort_list):
    for i in range(len(unsort_list)-1):

        for j in range(i+1,len(unsort_list)):
            if unsort_list[i]>unsort_list[j]:
                temp = unsort_list[i]
                unsort_list[i] = unsort_list[j]
                unsort_list[j] = temp
    unsort_list = unsort_list[::-1]
   # print("bubble sorted list:{}".format(unsort_list))

t = time.time()
bubblesort(unsort_list)
print("bubble sorting time : " , time.time()-t)


def selection_sort(unsort_list):
    for i in range(len(unsort_list)):
        # We assume that the first item of the unsorted segment is the smallest
        lowest_value_index = i
        # This loop iterates over the unsorted items
        for j in range(i + 1, len(unsort_list)):
            if unsort_list[j] < unsort_list[lowest_value_index]:
                lowest_value_index = j
        # Swap values of the lowest unsorted element with the first unsorted
        # element
        unsort_list[i], unsort_list[lowest_value_index] = unsort_list[lowest_value_index], unsort_list[i]
        unsort_list = unsort_list[::-1]
    #print("selection sorted list:{}".format(unsort_list))
t1 = time.time()
selection_sort(unsort_list)
print("selection sorting time : ",  time.time()-t1)
